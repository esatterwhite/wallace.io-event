Wallace.io Event Messages
=========================
    var Event = require("wallace.io-event")
    var e = new Event('foobar', {channel:"foobardata" });
    e.write({'foo':'bar', 'test':'data'})
    e.write({'more':"data"})

    e.serialize('json')
    // '{"meta":{"event":"foobar","application":"foobardata","channel":"foobardata","server":{"host":"apollo","address":"10.115.50.141","name":"wallace.io"},"id":"49ce970b-78de-45c2-bd15-8c16a95ee9d0","timestamp":"Tue, 13 Aug 2013 16:56:38 CDT","time":1376430998},"data":{"foo":"bar","test":"data","more":"data"}}'

    e.serialize('xml')
    //'<?xml version="1.0" encoding="UTF-8"?>\n<event><meta><event>foobar</event><application>foobardata</application><channel>foobardata</channel><server><host>apollo</host><address>10.115.50.141</address><name>wallace.io</name></server><id>55dedaaf-f363-4c22-8066-fba4d5becede</id><timestamp>Tue, 13 Aug 2013 16:57:13 CDT</timestamp><time>1376431033</time></meta><data><foo>bar</foo><test>data</test><more>data</more></data></event>'
