/*jshint laxcomma:true, smarttabs: true */
/**
 * DESCRIPTION
 * @module lib/event
 * @author Eric Satterwhite
 * @requires path
 * @requires os
 * @requires node-uuid
 * @requires requirejs
 * @requires util/negotiate
 * @requires logging
 * @requires class
 * @requires jsonselect
 **/
var path = require('path')
	,negotiate	= require('wallace.io-negotiator' )	// content negotiator module
	,os			= require('os')  				// nodejs os module
	,uuid		= require('node-uuid')			// nodejs uuid module
	,jsonselect	= require("JSONSelect")
	,braveheart = require("braveheart")
	,basePath									// basepath to braveheart libraries
	,Class										// Class module
	,EventBase									// reference to this module class
	,date										// braveheart date module
	,jsonselect									// braveheart jsonselect module
	,formats;									// serialization format codes ( needed ? )


formats		= [ "json", "xml" ]

date		= braveheart("date");
object		= braveheart("object");


/**
 * Small wrapper to standardize event formats and serialization
 * @class module:lib/event.Event
 * @param {String} event The name of the event
 * @param {Object} options The options for the class instace
 */
EventBase = function( evt, opts )/** @lends module:lib/event.Event.prototype */{
	opts = opts || {};
	this.evt     = evt;
	this.channel = opts.channel;
	this.data    =  opts.data || {};
}

/**
 * Serializes the bundle into the specified format
 * @method module:lib/event.Event#serialize
 * @param {String} [format=json] The format to serialize the data to. Can be xml or json
 * @return {String} The serialized Event bundle
 **/
EventBase.prototype.serialize = function( format ){
	var bundle = this.bundle( this.data )
	return negotiate.encode( bundle, format, "event");
}

/**
 * Writes data to the event body
 * @method module:lib/event.Event#write
 * @param {Object} [data] The data object to write to the event
 **/
EventBase.prototype.write = function( data ){
	this.data = object.merge( this.data, ( data || {} ) )
	return this;
}
/**
 * Does the work of packaging associated data with the current event
 * @method module:lib/event.Event#bundle
 * @param {Object} data The data object to bundle
 * @return {Object} The final message to be serialized
 **/
EventBase.prototype.bundle = function( data ){
	data = data || this.data;
	var ifaces = os.networkInterfaces()
		, addr = jsonselect.match( '.eth0 object:has(.family:val("IPv4")) > .address', ifaces )[0]
		, now = new Date()
		, message = {
			meta:{
				event:this.evt
				,application: this.channel
				,channel: this.channel
				,server:{
					host:os.hostname()
					,address:addr || "unknown address"
					,name:"wallace.io"
				}
				,id:uuid.v4()
				,timestamp:date.format( now, "%a, %d %b %Y %H:%M:%S %Z")
				,time: + ( now/1E3|0 )

			}
			,data:( data )
		};

	return message
}


module.exports = EventBase;
